const jsonServer = require('json-server');
const request = require('request');
const server = jsonServer.create();
const router = jsonServer.router('res.json');
const middlewares = jsonServer.defaults();

const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

server.use(middlewares);

var adapter = new FileSync('res.json');
var db = low(adapter);

var bodyParser = require('body-parser');
server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());

server.get('/sample', (req, res) => {
	console.log(_smsId);
	var resultJson = db.get('return').value();
	res.set('Content-Type', 'application/json');
	res.json(resultJson);
})

server.use(router);

server.listen(3003, () => {
});